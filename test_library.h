///////////////////////////////////////////////////////////////////////////////
/// @file test_library.h
/// @author David Norton :: CS153 Section 1A
/// @brief This is the header for the Test Library class,
/// which does unit testing on the library that performs sorting and other
/// useful functions
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @class Test_library
/// @brief Design to unit test the sorting library.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_library);
/// @brief This calls the unit testing library for CPP,
/// using this header file as an argument
/// @pre Requires the indicated header file to exist
/// @post Reports the success or failure of unit tests.
/// @param Test_array is the header being called
/// @return I believe this is void, instead cout-ing
/// the results to the terminal
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_bogosort);
/// @brief This calls the unit testing library for CPP, 
/// using the test_bogosort member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_bogosort is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_quantum_bogosort);
/// @brief This calls the unit testing library for CPP, 
/// using the test_quantum_bogosort member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_quantum_bogosort is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_search);
/// @brief This calls the unit testing library for CPP, 
/// using the test_search member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_search is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_binary_search);
/// @brief This calls the unit testing library for CPP, 
/// using the test_binary_search member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_binary_search is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_bubble_sort);
/// @brief This calls the unit testing library for CPP, 
/// using the test_bubble_sort member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_bubble_sort is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_selection_sort);
/// @brief This calls the unit testing library for CPP, 
/// using the test_selection_sort member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_selection_sort is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_insertion_sort);
/// @brief This calls the unit testing library for CPP, 
/// using the test_insertion_sort member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_insertion_sort is the function being called
/// @return True/false based upon success or failure of test.
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_print);
/// @brief This calls the unit testing library for CPP, 
/// using the test_print member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_print is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_fill);
/// @brief This calls the unit testing library for CPP, 
/// using the test_fill member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_fill is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_shuffle);
/// @brief This calls the unit testing library for CPP, 
/// using the test_shuffle member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_shuffle is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_ordered);
/// @brief This calls the unit testing library for CPP, 
/// using the test_ordered member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_ordered is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn void test_bogosort();
/// @brief This is a dummy test of the bogosort -- no sane person would
/// actually conduct a bogosort, but I thought I'd write it up for fun.
/// This test actually does nothing.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn void test_quantum_bogosort();
/// @brief This is a dummy test of the quantum bogosort -- no non-genocidal
/// person would actually conduct a quantum bogosort, but I thought I'd 
/// write it up for fun. This test actually does nothing.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn void test_search();
/// @brief This tests the correctness of the search function. It instantiates
/// a vector, fills it and makes sure that the search function can find 
/// everything in it. It also tests for attempting to find something not
/// currently in the vector, which should throw an ITEM_NOT_FOUND exception.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn void test_binary_search();
/// @brief This tests the correctness of the binary_search function. It 
/// instantiates a vector, fills it and makes sure that the search function 
/// can find everything in it. It also tests for attempting to find 
/// something not currently in the vector, which should throw an 
/// ITEM_NOT_FOUND exception.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_bubble_sort();
/// @brief This tests the correctness of the bubble sort function. It 
/// instantiates a vector, fills it, shuffles it, and then has the bubble sort
/// create order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_selection_sort();
/// @brief This tests the correctness of the selection sort function. It 
/// instantiates a vector, fills it, shuffles it, and then the selection sort
/// creates order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_insertion_sort();
/// @brief This tests the correctness of the insertion sort function. It 
/// instantiates a vector, fills it, shuffles it, and then the insertion sort
/// creates order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_print();
/// @brief This tests the correctness of the print function, by, well, printing
/// a vector so I can look at it myself. This is useful for debugging purposes.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_fill();
/// @brief This tests the correctness of the fill function by testing the
/// vector that has been filled -- it should be the proper size and be ordered.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_shuffle();
/// @brief This tests correctness of the shuffle function, by, well, printing
/// a vector so I can look at it myself. It instantiates a vector, fills it,
/// shuffles it, and then prints the shuffled vector.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_ordered();
/// @brief This tests correctness of the ordered vector function.
/// It instantiates a vector, fills it, and shows that the vector is already
/// ordered from least to greatest. It then performs a swap so that it is not
/// in order, and finally demonstrates that the ordered_vector properly reports
/// that the vector is not ordered least to greatest.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_LIBRARY_H
#define TEST_LIBRARY_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "library.h"

class Test_library : public CPPUNIT_NS::TestFixture
{
  private:
    CPPUNIT_TEST_SUITE (Test_library);

      //CPPUNIT_TEST (test_bogosort);
      //CPPUNIT_TEST (test_quantum_bogosort);

      CPPUNIT_TEST (test_search);
      CPPUNIT_TEST (test_binary_search);
      CPPUNIT_TEST (test_bubble_sort);
      CPPUNIT_TEST (test_selection_sort);
      CPPUNIT_TEST (test_insertion_sort);

      //CPPUNIT_TEST (test_print);
      //CPPUNIT_TEST (test_fill);
      //CPPUNIT_TEST (test_shuffle);
      //CPPUNIT_TEST (test_ordered);
      
    CPPUNIT_TEST_SUITE_END ();

  protected:
    void test_bogosort();
    void test_quantum_bogosort();

    void test_search();
    void test_binary_search();
    void test_bubble_sort();
    void test_selection_sort();
    void test_insertion_sort();
    
    void test_print();
    void test_fill();
    void test_shuffle();
    void test_ordered();


    
};

#endif
