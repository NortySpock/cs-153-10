//////////////////////////////////////////////////////////////////////////////
/// @file main.cpp
/// @author David Norton :: CS153 Section 1A
/// @brief This is the main file
//////////////////////////////////////////////////////////////////////////////

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cstdlib>
#include <ctime>
using namespace std;


//////////////////////////////////////////////////////////////////////
/// @fn int main()
/// @brief The core of the program. This calls the Cpp Unit tester, which
/// calls everything else.
/// @pre Program not running.
/// @post Program finished.
/// @param None
/// @return int representing program completion status.
//////////////////////////////////////////////////////////////////////

int main ()
{
  srand(time(0));

  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry
      = CppUnit::TestFactoryRegistry::getRegistry();

  runner.addTest (registry.makeTest ());

  return runner.run ("", false);
}
