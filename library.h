///////////////////////////////////////////////////////////////////////////////
/// @file library.h
/// @author David Norton :: CS153 Section 1A
/// @brief This is the header for the Library class,
/// which provides searching, sorting and other useful functions for vectors
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @class library
/// @brief Provides useful functions 
///////////////////////////////////////////////////////////////////////////////

#ifndef LIBRARY_H
#define LIBRARY_H

#include "exception.h"
#include "vector.h"
#include <cstdlib>

/*
 * Required functions
 */

///////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int search (Vector<generic> &, generic);
/// @brief Searches for the given item in the vector given. If found, it
/// the index of that item. Otherwise, it throws an ITEM_NOT_FOUND
/// exception.
/// @pre Vector exists
/// @post Function returns index of item, or throws exception if not found.
/// @param Vector to be search, item to be found 
/// @return unsigned int of inded of item.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int search (Vector<generic> &, generic);

///////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int binary_search (Vector<generic> &, generic);
/// @brief Searches for the given item in the vector given. If found, it
/// the index of that item. Otherwise, it throws an ITEM_NOT_FOUND
/// exception. This algorithm is more efficient than the regular search.
/// @pre Vector exists
/// @post Function returns index of item, or throws exception if not found.
/// @param Vector to be search, item to be found 
/// @return unsigned int of inded of item.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int binary_search (Vector<generic> &, generic);

///////////////////////////////////////////////////////////////////////////////
/// @fn void bubble_sort (Vector<generic> &);
/// @brief This uses a bubble sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void bubble_sort (Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void selection_sort (Vector<generic> &);
/// @brief This uses a selection sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void selection_sort (Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void insertion_sort (Vector<generic> &);
/// @brief This uses a insertion sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void insertion_sort (Vector<generic> &);

/*
 * Not required functions implemented for my own amusement.
 */

///////////////////////////////////////////////////////////////////////////////
/// @fn void bogosort (Vector<generic> &);
/// @brief "Sorts" the vector by shuffling it and then checking to see if it's 
/// sorted. Worst case runtime is indefinite!
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void bogosort (Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void quantum_bogosort (Vector<generic> &);
/// @brief An improvement on the bogosort, this function shuffles the vector,
/// then, if it is not sorted, it destroys the universe. Thus, from the
/// perspective of a programer who is in a parallel universe that was not 
/// destroyed, it works every time in O(2n).    
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void quantum_bogosort (Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void destroyTheUniverse();
/// @brief This destroys the universe.
/// @pre Universe exists.
/// @post Universe does not exist.
/// @param None.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
void destroyTheUniverse(); //I guess that would be a void function, huh?

/*
 * Not required functions implemented to make testing easier.
 */

///////////////////////////////////////////////////////////////////////////////
/// @fn void print_vector (Vector<generic> &);
/// @brief Outputs the content of the vector for visual examiniation.
/// @pre Vector exists.
/// @post Content of vector is output to console.
/// @param Vector to be printed.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void print_vector (Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void fill_vector (Vector<generic> &);
/// @brief Clears vector and fills it from zero to length minus one.
/// @pre Vector exists.
/// @post Vector contains integers from 0 to length - 1
/// @param Vector to be filled, length to fill it to.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void fill_vector(Vector<generic> &, int length);

///////////////////////////////////////////////////////////////////////////////
/// @fn void shuffle_vector (Vector<generic> &);
/// @brief Implements a Knuth shuffle on the vector.
/// @pre Vector exists.
/// @post Vector's contents are randomly shuffled.
/// @param Vector to be shuffled.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void shuffle_vector(Vector<generic> &);

///////////////////////////////////////////////////////////////////////////////
/// @fn void swap_vector (Vector<generic> &);
/// @brief Swaps the contents at two index values in a vector
/// @pre Vector exists, two legal index positions are given.
/// @post Contents at the listed index positions are swapped.
/// @param Vector to have values swapped, first and second indexes to swap.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void swap_vector(Vector<generic> &, int first, int second);

///////////////////////////////////////////////////////////////////////////////
/// @fn void ordered_vector (Vector<generic> &);
/// @brief Checks to see if vector is, in fact, ordered from least to greatest.
/// @pre Vector exists.
/// @post N/A
/// @param Vector to be checked
/// @return True if vector is in least to greatest order, false if not.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
bool ordered_vector(Vector<generic>);

#include "library.hpp"
#endif
